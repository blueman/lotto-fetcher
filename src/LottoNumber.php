<?php

namespace BlueMan\Lotto;

class LottoNumber
{
    public $name;

    /** @var \DateTime */
    private $date;

    /** @var array */
    private $numbers = [];

    /** @var array */
    private $plusNumbers = [];

    public function __construct($name, \DateTime $date = null, $numbers = [], $plusNumbers = [])
    {
        $this->name = $name;
        $this->date = $date;
        $this->numbers = $numbers;
        $this->plusNumbers = $plusNumbers;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return array
     */
    public function getNumbers()
    {
        return $this->numbers;
    }

    /**
     * @param array $numbers
     */
    public function setNumbers($numbers)
    {
        $this->numbers = $numbers;
    }

    /**
     * @return array
     */
    public function getPlusNumbers()
    {
        return $this->plusNumbers;
    }

    /**
     * @param array $plusNumbers
     */
    public function setPlusNumbers($plusNumbers)
    {
        $this->plusNumbers = $plusNumbers;
    }
}
