<?php

namespace BlueMan\Lotto;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class LottoFetcher
{
    public $TYPE_LOTTO = 'lotto';
    public $TYPE_LOTTOMINI = 'lotto-mini';
    public $TYPE_EKSTRAPENSJA = 'ekstra-pensja';
    public $TYPE_KASKADA = 'kaskada';
    public $TYPE_MULTIMULTI = 'multi-multi';
    public $TYPE_KENO = 'keno';

    private $lottoPageHtml;

    /**
     * Creating my HTML fetcher client.
     */
    private function pageContentFetcher()
    {
        if (is_null($this->lottoPageHtml)) {
            $client = new Client(
                [
                    'base_uri' => 'http://www.lotto.pl',
                    'timeout' => 2.0,
                ]
            );
            $response = $client->get('wyniki-gier');

            $this->lottoPageHtml = $response->getBody()->getContents();
        }
    }

    /**
     * Get list of numbers for Lotto game with Plus numbers.
     *
     * @return LottoNumber
     */
    public function getLottoNumbers()
    {
        $mainLottos = $this->mainHtmlFetcher($this->TYPE_LOTTO, '.glowna_wyniki_lottoplus > .wynik_lottoplus');

        $lottoNumbers = [];
        $lottoPlusNumbers = [];
        for ($i = 0; $i < 6; ++$i) {
            $lottoNumbers[] = trim($mainLottos['numbers']->eq($i)->text());
            $lottoPlusNumbers[] = trim($mainLottos['plus_numbers']->eq($i)->text());
        }

        $lottoNumber = new LottoNumber($this->TYPE_LOTTO, $mainLottos['date'], $lottoNumbers, $lottoPlusNumbers);

        return $lottoNumber;
    }

    /**
     * Get list of numbers for Mini Lotto game.
     *
     * @return LottoNumber
     */
    public function getMiniLottoNumbers()
    {
        $lottoNumber = $this->commonHtmlFetcher($this->TYPE_LOTTOMINI, 5);

        return $lottoNumber;
    }

    /**
     * Get list of numbers for Pensja game with extra number.
     *
     * @return LottoNumber
     */
    public function getExtraPensjaNumbers()
    {
        $lottoNumber = $this->commonHtmlFetcher($this->TYPE_EKSTRAPENSJA, 5, '.glowna_wyniki_ekstra-pensja > .wynik_ekstra_pensja');

        return $lottoNumber;
    }

    /**
     * Get list of numbers for Kaskada game.
     *
     * @return LottoNumber
     */
    public function getKaskadaNumbers()
    {
        $lottoNumber = $this->commonHtmlFetcher($this->TYPE_KASKADA, 12);

        return $lottoNumber;
    }

    /**
     * Get list of numbers for Multi Multi game with plus number.
     *
     * @return LottoNumber
     */
    public function getMultiMultiNumbers()
    {
        $lottoNumber = $this->commonHtmlFetcher($this->TYPE_MULTIMULTI, 20, '.wynik_multi-multi_plus');

        return $lottoNumber;
    }

    /**
     * Get list of numbers for Keno game.
     *
     * @return LottoNumber
     */
    public function getKenoNumbers()
    {
        $lottoNumber = $this->commonHtmlFetcher($this->TYPE_KENO, 20);

        return $lottoNumber;
    }

    /**
     * Main HTML fetcher from website with easy/dummy DOM crawler.
     *
     * @param string      $gameName          game name used to extract the DOM
     * @param string|null $plusNumberHandler OPTIONAL extra DOM handler for additional plus number for some games
     *
     * @return array
     */
    private function mainHtmlFetcher($gameName, $plusNumberHandler = null)
    {
        $this->pageContentFetcher();

        $crawler = new Crawler($this->lottoPageHtml);

        $lottoResults = $crawler->filter('.start-wyniki_'.$gameName);
        $lottoDates = $lottoResults->filter('.wyniki_data > strong');
        $lottoDate = $lottoDates->eq(0)->text().' '.$lottoDates->eq(1)->text();
        $lastLottoDate = Carbon::createFromFormat('d-m-y H:i', $lottoDate, 'Europe/Warsaw');

        $lottoNumbersDom = $lottoResults->filter('.glowna_wyniki_'.$gameName.' > .wynik_'.$gameName);
        $lottoPlusNumbersDom = is_null($plusNumberHandler) ? null : $lottoResults->filter($plusNumberHandler);

        return [
            'date' => $lastLottoDate,
            'numbers' => $lottoNumbersDom,
            'plus_numbers' => $lottoPlusNumbersDom,
        ];
    }

    /**
     * Assigning the list of number to LottNumber class.
     *
     * @param string      $gameName          game name
     * @param int         $numbers           numbers used by the game
     * @param string|null $plusNumberHandler OPTIONAL extra DOM handler for additional plus number for some games
     *
     * @return LottoNumber
     */
    private function commonHtmlFetcher($gameName, $numbers, $plusNumberHandler = null)
    {
        $mainLottos = $this->mainHtmlFetcher($gameName, $plusNumberHandler);

        $lottoNumbers = [];
        $lottoPlusNumbers = [];
        for ($i = 0; $i < $numbers; ++$i) {
            $lottoNumbers[] = trim($mainLottos['numbers']->eq($i)->text());
        }

        if (!is_null($plusNumberHandler)) {
            $lottoPlusNumbers[] = trim($mainLottos['plus_numbers']->filter($plusNumberHandler)->text());
        }

        $lottoNumber = new LottoNumber($gameName, $mainLottos['date'], $lottoNumbers, $lottoPlusNumbers);

        return $lottoNumber;
    }
}
